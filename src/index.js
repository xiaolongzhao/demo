import React, { Component } from 'react';
import 'core-js/fn/object/assign';
import { render } from 'react-dom';
import App from './components/Main';

render((<App />), document.getElementById('app'));

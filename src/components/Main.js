import React, { PropTypes, Component } from 'react';
import {ListGroup,ListGroupItem} from 'react-bootstrap';
import InfiniteScroll from 'react-infinite-scroller';

export default class AppComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [this.createItem(0)],
      hasMoreItems: true,
    };
  }
  createItem(page) {
    return ( < ListGroupItem key={page}> {
        'Hello page ' + page + ' !'
      } </ListGroupItem>
    );
  }
  loadItems(page) {
    /*setTimeout(function () {
      this.setState({
        items: this.state.items.concat([this.createItem(page)]),
        hasMoreItems: (page < 15)
      });
    }.bind(this), 1000);*/
    this.setState({
      items: this.state.items.concat([this.createItem(page)]),
      hasMoreItems: (page < 30)
    });
  }
  render() {
    const loader = <div className="loader">Loading ...</div>;
    const{items,hasMoreItems} = this.state;
    return (
      <InfiniteScroll
        pageStart={0}
        loadMore={this.loadItems.bind(this)}
        hasMore={hasMoreItems}
        loader={loader}>
        <ListGroup>{items}</ListGroup>
      </InfiniteScroll>
    );
  }
}

